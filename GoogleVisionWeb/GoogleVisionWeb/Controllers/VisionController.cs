﻿using Google.Cloud.Vision.V1;
using GoogleVision.Logic.Logic;
using GoogleVision.Logic.Model;
using GoogleVision.Logic.Model.FaceDetection;
using GoogleVision.Logic.Model.SafeSearch;
using GoogleVision.Logic.Model.WebDetection;
using GoogleVision.Web.Helpers;
using GoogleVision.Web.Models;
using GoogleVision.Web.Models.Vison;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace GoogleVisionWeb.Controllers
{
    public class VisionController : ApiController
    {
        private GoogleVisionPictureAnalysisLogic _visionLogic;

        public VisionController()
        {
            _visionLogic = new GoogleVisionPictureAnalysisLogic();
        }

        [HttpPost]
        public bool ParseImage()
        {
            if (HttpContext.Current.Request.Files != null && HttpContext.Current.Request.Files.Count != 0)
            {
                HttpPostedFile uploadedFile = HttpContext.Current.Request.Files[0];
                string fileName = uploadedFile.FileName;
                byte[] file = new byte[uploadedFile.ContentLength];
                uploadedFile.InputStream.Read(file, 0, uploadedFile.ContentLength);

                Image img = Image.FromBytes(file);

                AnnotateImageRequest request = new AnnotateImageRequest()
                {
                    Image = img
                };
                var labelDetectionFeature = new Feature();
                labelDetectionFeature.Type = Feature.Types.Type.LabelDetection;

                var webDetectionFeature = new Feature();
                webDetectionFeature.Type = Feature.Types.Type.WebDetection;

                var saveSearchFeature = new Feature();
                saveSearchFeature.Type = Feature.Types.Type.SafeSearchDetection;

                var faceSearchFeature = new Feature();
                faceSearchFeature.Type = Feature.Types.Type.FaceDetection;

                request.Features.Add(labelDetectionFeature);
                request.Features.Add(webDetectionFeature);
                request.Features.Add(saveSearchFeature);
                request.Features.Add(faceSearchFeature);

                AnnotateImageResponse res = ImageAnnotatorClient.Create().Annotate(request);

                LabelDetectionDto labelAnnotations = res.LabelAnnotations == null ?
                                                        null :
                                                        new LabelDetectionDto(res.LabelAnnotations.Select(n => new LabelAnnotationDto(n.Description, n.Score)).ToList());

                WebDetectionDto webAnnotations = res.WebDetection == null ?
                                                        null :
                                                        new WebDetectionDto(
                                                            res.WebDetection.WebEntities.Select(n => new WebSimilarSearchAnnotationDto(n.Score, n.Description)).ToList(),
                                                            res.WebDetection.PagesWithMatchingImages.Select(n => n.Url).ToList(),
                                                            res.WebDetection.FullMatchingImages.Select(n => n.Url).ToList(),
                                                            res.WebDetection.PartialMatchingImages.Select(n => n.Url).ToList());

                SafeSearchAnnotationDto safeSearchAnnotations = res.SafeSearchAnnotation == null ?
                                                        null :
                                                        new SafeSearchAnnotationDto(
                                                            VisionAPIHelpers.ParseLilelihoodEnum(res.SafeSearchAnnotation.Adult),
                                                            VisionAPIHelpers.ParseLilelihoodEnum(res.SafeSearchAnnotation.Spoof),
                                                            VisionAPIHelpers.ParseLilelihoodEnum(res.SafeSearchAnnotation.Medical),
                                                            VisionAPIHelpers.ParseLilelihoodEnum(res.SafeSearchAnnotation.Violence));

                FaceDetectionDto faceDetection = res.FaceAnnotations == null ?
                                                        null :
                                                        new FaceDetectionDto(res.FaceAnnotations.Select(n => new FaceSquareDto(
                                                                new PointDto(n.FdBoundingPoly.Vertices[0].X, n.FdBoundingPoly.Vertices[0].Y),
                                                                new PointDto(n.FdBoundingPoly.Vertices[1].X, n.FdBoundingPoly.Vertices[1].Y),
                                                                new PointDto(n.FdBoundingPoly.Vertices[2].X, n.FdBoundingPoly.Vertices[2].Y),
                                                                new PointDto(n.FdBoundingPoly.Vertices[3].X, n.FdBoundingPoly.Vertices[3].Y)))
                                                                .ToList());

                if (res.Error == null)
                {
                    _visionLogic.Add(new GoogleVisionPictureAnalysisDto(
                        file,
                        fileName,
                        labelAnnotations,
                        webAnnotations,
                        safeSearchAnnotations,
                        faceDetection));
                }
                else
                {
                    //TODO: handle error
                    return false;
                }
                return true;
            }
            else
            {
                //TODO: handle file missing error
                return false;
            }
        }

        [HttpGet]
        public VisionPageModel GetSavedImages()
        {

            return new VisionPageModel(
                _visionLogic.GetAll()
                .Select(n => new GoogleVisionAnalysisResultModel(
                    n.ID,
                    n.FileName,
                    n.File,
                    n.LabelAnnotations == null ?
                        new VisionLabelAnalysisResultModel() :
                        new VisionLabelAnalysisResultModel(n.LabelAnnotations.Annotations.Select(m => new VisionLabelAnnotationModel(m.Description, (int)(m.Score * 100))).ToList()),
                    n.WebAnnotations == null ?
                        new VisionWebAnalysisResultModel() :
                        new VisionWebAnalysisResultModel(
                            n.WebAnnotations.SimilarSearches.Select(m => new VisionWebSimilarSearchResultModel(m.Score, m.Description)).ToList(),
                            n.WebAnnotations.PagesWithMatchedImages,
                            n.WebAnnotations.SameImagesUrls,
                            n.WebAnnotations.SimilarImagesUrls),
                    n.SafeSearchAnnotations == null ?
                        new VisionSafeSearchAnalysisResultModel() :
                        new VisionSafeSearchAnalysisResultModel(
                            n.SafeSearchAnnotations.Adult,
                            n.SafeSearchAnnotations.Spoof,
                            n.SafeSearchAnnotations.Medical,
                            n.SafeSearchAnnotations.Violence),
                    n.FaceDetection == null ?
                        new GoogleVisionFaceDetectionResultModel() :
                        new GoogleVisionFaceDetectionResultModel(
                            n.FaceDetection.FaceSquare.Select(m => new GoogleVisionFaceSquareResultModel(
                               new GoogleVisionPointModel(m.LeftTop.X, m.LeftTop.Y),
                               new GoogleVisionPointModel(m.RightTop.X, m.RightTop.Y),
                               new GoogleVisionPointModel(m.LeftBottom.X, m.LeftBottom.Y),
                               new GoogleVisionPointModel(m.RightBottom.X, m.RightBottom.Y)))
                            .ToList())
                )).ToList());
        }

        [HttpPost]
        public void RemoveImage(RemoveImageModel model)
        {
            _visionLogic.Remove(_visionLogic.GetSingle(model.ID));
        }
    }


}
