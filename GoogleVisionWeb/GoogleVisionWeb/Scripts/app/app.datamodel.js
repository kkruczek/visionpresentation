﻿function AppDataModel() {
    var self = this;
    // Routes
    self.visionApiParseImage = "/api/Vision/ParseImage";
    self.visionApiGetSavedImages = "/api/Vision/GetSavedImages";
    self.visionApiRemoveImage = "/api/Vision/RemoveImage";


    self.siteUrl = "/";

    
    // Data
    self.returnUrl = self.siteUrl;
}
