﻿$(function () {
    app.initialize();

    // Activate Knockout
    ko.validation.init({ grouping: { observable: false } });
    ko.applyBindings(app);
});


jQuery(function ($) {
    $('.body-content').hide();
});
$(document).ready(function () {
    $('.body-content').fadeIn(300);
});