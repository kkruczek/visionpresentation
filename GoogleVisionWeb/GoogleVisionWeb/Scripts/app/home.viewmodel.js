﻿function HomeViewModel(app, dataModel) {
    var self = this;

    self.savedPictures = ko.observableArray([]);

    Sammy(function () {
        this.get('#home', function () {
            $(function () {
                Dropzone.options.dropzoneForm = {
                    maxFilesize: 2, // MB
                    createImageThumbnails: false,
                    acceptedFiles: 'image/*',
                    dictDefaultMessage: "Drop files here or click to upload",
                    init: function () {
                        this.on("complete", function (file) {
                            this.removeFile(file);
                            self.refreshList();
                            $('#overlay').fadeOut();
                        });
                        this.on("addedfile", function () {
                            $('#overlay').fadeIn();
                        });
                    }
                };
            });
            self.refreshList();
        });
        this.get('/', function () { this.app.runRoute('get', '#home') });
    });

    self.uploadImage = function () {
        $.ajax({
            method: 'post',
            url: app.dataModel.visionApiParseImage,
            contentType: "application/json; charset=utf-8",
            success: function (data) {

            }
        });
    };

    self.refreshList = function () {
        $.ajax({
            method: 'get',
            url: app.dataModel.visionApiGetSavedImages,
            contentType: "application/json; charset=utf-8",
            success: function (model) {
                self.savedPictures(ko.utils.arrayMap(model.visionAnalysisResults, function (picture) {
                    return new SavedPictureViewModel(app, picture, self);
                }));

                setTimeout(function () {

                    var $grid = $('.grid').masonry({
                        itemSelector: '.grid-item',
                        percentPosition: true,
                        columnWidth: '.grid-sizer'
                    });
                    $grid.masonry('destroy');
                    $grid = $('.grid').masonry({
                        itemSelector: '.grid-item',
                        percentPosition: true,
                        columnWidth: '.grid-sizer'
                    });
                }, 200);


                //// layout Masonry after each image loads
                //$grid.imagesLoaded().progress(function () {
                //    $grid.masonry();
                //});
            }
        });
    };

    return self;
}

function SavedPictureViewModel(app, dataModel, parentModel) {
    var self = this;
    var parent = parentModel;
    self.data = ko.observable(dataModel);
    self.img = ko.observable(dataModel.imgFull);

    console.log(self.data());

    self.showDetails = function () {
        ko.renderTemplate(
            "img-details-dialog",
            self,
            {
                afterRender: function (renderedElement) {
                    
                    $(document).keyup(function (e) {
                        if (e.keyCode == 27) { // escape key maps to keycode `27`
                            $('#fileinfo-modal').modal('hide');
                        }
                    });

                    $('#dlg-close').click(function () {
                        $('#fileinfo-modal').modal('hide');
                    });

                    if (self.data().faceAnalysis.exist) {
                        $('#describedImg').load(function () {
                            $('#faceCanvas').attr("width", $("#describedImg").width());
                            $('#faceCanvas').attr('height', $("#describedImg").height());
                            $('#faceCanvas').css('left', $('#describedImg').position().left);
                            //$('#faceCanvas').css('top', $('#describedImg').position().top);

                            var canvas = document.getElementById("faceCanvas");
                            var context = canvas.getContext('2d');
                            var resizeFactor = $('#describedImg').width() / self.data().imgWidth;

                            for (var i = 0; i < self.data().faceAnalysis.faces.length; i++) {
                                var face = self.data().faceAnalysis.faces[i];

                                context.beginPath();

                                context.moveTo(face.topLeft.x * resizeFactor, face.topLeft.y * resizeFactor);
                                context.lineTo(face.topRight.x * resizeFactor, face.topRight.y * resizeFactor);
                                context.lineTo(face.bottomLeft.x * resizeFactor, face.bottomLeft.y * resizeFactor);
                                context.lineTo(face.bottomRight.x * resizeFactor, face.bottomRight.y * resizeFactor);
                                context.lineTo(face.topLeft.x * resizeFactor, face.topLeft.y * resizeFactor);

                                context.lineWidth = 2;

                                context.strokeStyle = '#5bd82d';
                                context.stroke();
                            }
                        });
                    }
                }
            },
            $('#fileinfo-modal-cont'),
            "replaceNode"
        );

        $('#fileinfo-modal').modal({
            keyboard: true,
            show: true
        });

        $('#fileinfo-modal').on("hidden.bs.modal", function () {
            console.log("hidden");
            $('#fileinfo-modal').html('<div id="fileinfo-modal-cont"></div>');
        });


    };

    self.remove = function () {
        var imgId = self.data().id;
        $.ajax({
            method: 'post',
            url: app.dataModel.visionApiRemoveImage,
            contentType: "application/json; charset=utf-8",
            dataType: 'json',
            data: JSON.stringify({ ID: imgId }),
            success: function (model) {
                parent.refreshList();
            }
        });
    };
};

app.addViewModel({
    name: "Home",
    bindingMemberName: "home",
    factory: HomeViewModel
});
