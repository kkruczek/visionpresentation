﻿using GoogleVision.Web.Models.Vison.Bases;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GoogleVision.Web.Models.Vison
{
    public class VisionLabelAnalysisResultModel : VisionAnalysisResultBaseModel
    {
        public List<VisionLabelAnnotationModel> LabelAnnotations { get; set; }

        public VisionLabelAnalysisResultModel()
            :base(false)
        { }

        public VisionLabelAnalysisResultModel(List<VisionLabelAnnotationModel> annotations)
            :base(true)
        {
            this.LabelAnnotations = annotations;
        }
    }
}