﻿using GoogleVision.Web.Models.Vison.Bases;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GoogleVision.Web.Models.Vison
{
    public class GoogleVisionFaceDetectionResultModel : VisionAnalysisResultBaseModel
    {
        public List<GoogleVisionFaceSquareResultModel> Faces { get; set; }

        public GoogleVisionFaceDetectionResultModel ()
            : base(false)
        {
        }

        public GoogleVisionFaceDetectionResultModel(List<GoogleVisionFaceSquareResultModel> faces)
            :base(true)
        {
            this.Faces = faces;
        }
    }
}