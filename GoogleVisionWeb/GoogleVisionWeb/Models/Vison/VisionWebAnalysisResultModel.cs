﻿using GoogleVision.Web.Models.Vison.Bases;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GoogleVision.Web.Models.Vison
{
    public class VisionWebAnalysisResultModel : VisionAnalysisResultBaseModel
    {
        public List<VisionWebSimilarSearchResultModel> SimilarSearches { get; set; }
        
        public List<string> PagesWithMatchedImages { get; set; }
        public List<string> SameImagesUrls { get; set; }
        public List<string> SimilarImagesUrls { get; set; }

        public VisionWebAnalysisResultModel()
            : base(false)
        {
            this.SimilarSearches = new List<VisionWebSimilarSearchResultModel>();
            this.PagesWithMatchedImages = new List<string>();
            this.SameImagesUrls = new List<string>();
            this.SimilarImagesUrls = new List<string>();
        }

        public VisionWebAnalysisResultModel(
            List<VisionWebSimilarSearchResultModel> similarSearches,
            List<string> matchedImages,
            List<string> sameIamges,
            List<string> similarImages)
            : base(true)
        {
            this.SimilarSearches = similarSearches;
            this.PagesWithMatchedImages = matchedImages;
            this.SameImagesUrls = sameIamges;
            this.SimilarImagesUrls = similarImages;
        }
    }
}