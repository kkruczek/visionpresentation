﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GoogleVision.Web.Models.Vison
{
    public class GoogleVisionPointModel
    {
        public int X { get; set; }
        public int Y { get; set; }

        public GoogleVisionPointModel(int x, int y)
        {
            this.X = x;
            this.Y = y;
        }
    }
}