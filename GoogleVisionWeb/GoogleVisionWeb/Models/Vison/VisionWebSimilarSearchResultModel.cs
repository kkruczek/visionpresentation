﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GoogleVision.Web.Models.Vison
{
    public class VisionWebSimilarSearchResultModel
    {
        public float Score { get; set; }
        public string Description { get; set; }

        public VisionWebSimilarSearchResultModel(float score, string description)
        {
            this.Score = score;
            this.Description = description;
        }
    }
}