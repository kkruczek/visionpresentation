﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GoogleVision.Web.Models.Vison.Bases
{
    public class VisionAnalysisResultBaseModel
    {
        public bool Exist { get; set; }

        public VisionAnalysisResultBaseModel(bool exist)
        {
            this.Exist = exist;
        }
    }
}