﻿using GoogleVision.Web.Helpers;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;

namespace GoogleVision.Web.Models.Vison
{
    public class GoogleVisionAnalysisResultModel
    {
        public int ID { get; set; }
        public string FileName { get; set; }

        public VisionLabelAnalysisResultModel LabelAnalysis { get; set; }
        public VisionWebAnalysisResultModel WebInfoAnalysis { get; set; }
        public VisionSafeSearchAnalysisResultModel SaveSearchAnalysis { get; set; }
        public GoogleVisionFaceDetectionResultModel FaceAnalysis { get; set; }

        private string _imgFullPath;
        public string ImgFull { get { return _imgFullPath; } }
        public int ImgHeight
        {
            get
            {
                return Image.FromFile(System.Web.HttpContext.Current.Server.MapPath(_imgFullPath)).Height;
            }
        }
        public int ImgWidth
        {
            get
            {
                return Image.FromFile(System.Web.HttpContext.Current.Server.MapPath(_imgFullPath)).Width;
            }
        }

        public GoogleVisionAnalysisResultModel(
            int id,
            string fileName,
            byte[] file,
            VisionLabelAnalysisResultModel labelAnalysis,
            VisionWebAnalysisResultModel webInfoAnalysis,
            VisionSafeSearchAnalysisResultModel saveSearchAnalysis,
            GoogleVisionFaceDetectionResultModel faceAnalysis)
        {
            FileProcessor.EnshureFileExist(fileName, file);
            _imgFullPath = FileProcessor.GetFilePathForWeb(fileName);
            this.ID = id;
            this.FileName = fileName;
            this.LabelAnalysis = labelAnalysis;
            this.WebInfoAnalysis = webInfoAnalysis;
            this.SaveSearchAnalysis = saveSearchAnalysis;
            this.FaceAnalysis = faceAnalysis;
        }
    }
}