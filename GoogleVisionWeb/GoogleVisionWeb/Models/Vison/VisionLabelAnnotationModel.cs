﻿namespace GoogleVision.Web.Models.Vison
{
    public class VisionLabelAnnotationModel
    {
        public string Description { get; set; }
        public int Score { get; set; }

        public VisionLabelAnnotationModel(string description, int score)
        {
            this.Description = description;
            this.Score = score;
        }
    }
}