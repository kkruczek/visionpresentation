﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GoogleVision.Web.Models.Vison
{
    public class GoogleVisionFaceSquareResultModel
    {
        public GoogleVisionPointModel TopLeft { get; set; }
        public GoogleVisionPointModel TopRight { get; set; }
        public GoogleVisionPointModel BottomLeft { get; set; }
        public GoogleVisionPointModel BottomRight { get; set; }

        public GoogleVisionFaceSquareResultModel(
            GoogleVisionPointModel topLeft,
            GoogleVisionPointModel topRight,
            GoogleVisionPointModel bottomLeft,
            GoogleVisionPointModel bottomRight)
        {
            this.TopLeft = topLeft;
            this.TopRight = topRight;
            this.BottomLeft = bottomLeft;
            this.BottomRight = bottomRight;
        }
    }
}