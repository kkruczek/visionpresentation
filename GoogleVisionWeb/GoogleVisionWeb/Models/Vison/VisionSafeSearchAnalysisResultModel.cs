﻿using GoogleVision.Logic.Model.SafeSearch;
using GoogleVision.Web.Models.Vison.Bases;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GoogleVision.Web.Models.Vison
{
    public class VisionSafeSearchAnalysisResultModel : VisionAnalysisResultBaseModel
    {
        public Likelihood Adult { get; set; }
        public Likelihood Spoof { get; set; }
        public Likelihood Medical { get; set; }
        public Likelihood Violence { get; set; }

        public VisionSafeSearchAnalysisResultModel()
            : base(false)
        { }

        public VisionSafeSearchAnalysisResultModel(
            Likelihood adult,
            Likelihood spoof,
            Likelihood medical,
            Likelihood violence)
            : base(true)
        {
            this.Adult = adult;
            this.Spoof = spoof;
            this.Medical = medical;
            this.Violence = violence;
        }
    }
}