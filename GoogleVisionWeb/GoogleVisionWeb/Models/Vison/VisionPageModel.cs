﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GoogleVision.Web.Models.Vison
{
    public class VisionPageModel
    {
        public List<GoogleVisionAnalysisResultModel> VisionAnalysisResults { get; set; }

        public VisionPageModel(List<GoogleVisionAnalysisResultModel> visionAnalysisResult)
        {
            this.VisionAnalysisResults = visionAnalysisResult;
        }
    }
}