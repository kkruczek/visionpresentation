﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Web;

namespace GoogleVision.Web.Helpers
{
    public static class FileProcessor
    {
        private const string _filesFolder = "files";
        private const string _filesMiniFolder = "files/filesMini";

        private const int _thumbnailImgWidth = 300;

        public static string FilesFolderPath
        {
            get
            {
                //enshure directory exist
                string folderPath = HttpContext.Current.Server.MapPath(string.Format("~/{0}", _filesFolder));
                if (!Directory.Exists(folderPath))
                {
                    Directory.CreateDirectory(folderPath);
                }

                return folderPath;
            }
        }

        public static string FilesMiniFolderPath
        {
            get
            {
                //enshure directory exist
                string folderPath = HttpContext.Current.Server.MapPath(string.Format("~/{0}", _filesMiniFolder));
                if (!Directory.Exists(folderPath))
                {
                    Directory.CreateDirectory(folderPath);
                }

                return folderPath;
            }
        }

        public static void EnshureFileExist(string fileName, byte[] file)
        {
            string filePath = Path.Combine(FilesFolderPath, fileName);
            if (!File.Exists(filePath))
            {
                using (FileStream fs = File.Create(filePath))
                {
                    fs.Write(file, 0, file.Length);
                    fs.Flush();
                }
            }

            string fileThumbnailPath = Path.Combine(FilesMiniFolderPath, fileName);
            if (!File.Exists(fileThumbnailPath))
            {
                using (MemoryStream ms = new MemoryStream(file))
                {
                    Image img = Image.FromStream(ms);
                    Image resizedImg = img.GetThumbnailImage(_thumbnailImgWidth, ((img.Height * _thumbnailImgWidth) / img.Width), null, IntPtr.Zero);
                    using (FileStream fs = File.Create(fileThumbnailPath))
                    {
                        resizedImg.Save(fs, ImageFormat.Png);
                        fs.Flush();
                    }
                }
            }
        }

        public static string GetFilePathForWeb(string fileName)
        {
            return VirtualPathUtility.ToAbsolute(string.Format("~/{0}/{1}", _filesFolder, fileName));
        }

        public static string GetFilePathMiniForWeb(string fileName)
        {
            return VirtualPathUtility.ToAbsolute(string.Format("~/{0}/{1}", _filesMiniFolder, fileName));
        }
    }
}