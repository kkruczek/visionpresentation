﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GoogleVision.Web.Helpers
{
    public static class VisionAPIHelpers
    {
        public static Logic.Model.SafeSearch.Likelihood ParseLilelihoodEnum(Google.Cloud.Vision.V1.Likelihood source)
        {
            switch (source)
            {
                default:
                case Google.Cloud.Vision.V1.Likelihood.Unknown:
                    return Logic.Model.SafeSearch.Likelihood.UNKNOWN;
                case Google.Cloud.Vision.V1.Likelihood.VeryUnlikely:
                    return Logic.Model.SafeSearch.Likelihood.VERY_UNLIKELY;
                case Google.Cloud.Vision.V1.Likelihood.Unlikely:
                    return Logic.Model.SafeSearch.Likelihood.UNLIKELY;
                case Google.Cloud.Vision.V1.Likelihood.Possible:
                    return Logic.Model.SafeSearch.Likelihood.POSSIBLE;
                case Google.Cloud.Vision.V1.Likelihood.Likely:
                    return Logic.Model.SafeSearch.Likelihood.LIKELY;
                case Google.Cloud.Vision.V1.Likelihood.VeryLikely:
                    return Logic.Model.SafeSearch.Likelihood.VERY_LIKELY;
            }
        }
    }
}