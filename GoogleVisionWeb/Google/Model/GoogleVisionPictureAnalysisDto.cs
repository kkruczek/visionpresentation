﻿using GoogleVision.Logic.Model.FaceDetection;
using GoogleVision.Logic.Model.SafeSearch;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GoogleVision.Logic.Model
{
    public class GoogleVisionPictureAnalysisDto
    {
        public int ID { get; set; }
        public byte[] File { get; set; }
        public string FileName { get; set; }

        public LabelDetectionDto LabelAnnotations { get; set; }
        public WebDetectionDto WebAnnotations { get; set; }
        public SafeSearchAnnotationDto SafeSearchAnnotations { get; set; }
        public FaceDetectionDto FaceDetection { get; set; }

        public GoogleVisionPictureAnalysisDto(
            byte[] file,
            string fileName,
            LabelDetectionDto labelAnnotations,
            WebDetectionDto webDetection,
            SafeSearchAnnotationDto safeSearch,
            FaceDetectionDto faceDetection)
        {
            this.File = file;
            this.FileName = fileName;

            this.LabelAnnotations = labelAnnotations;
            this.WebAnnotations = webDetection;
            this.SafeSearchAnnotations = safeSearch;
            this.FaceDetection = faceDetection;
        }
    }
}
