﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GoogleVision.Logic.Model.SafeSearch
{
    public enum Likelihood
    {
        UNKNOWN,
        VERY_UNLIKELY,
        UNLIKELY,
        POSSIBLE,
        LIKELY,
        VERY_LIKELY
    }

    public class SafeSearchAnnotationDto
    {
        public Likelihood Adult { get; set; }
        public Likelihood Spoof { get; set; }
        public Likelihood Medical { get; set; }
        public Likelihood Violence { get; set; }

        public SafeSearchAnnotationDto(
            Likelihood adult,
            Likelihood spoof,
            Likelihood medical,
            Likelihood violence)
        {
            this.Adult = adult;
            this.Spoof = spoof;
            this.Medical = medical;
            this.Violence = violence;
        }
    }
}
