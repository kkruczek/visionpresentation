﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GoogleVision.Logic.Model.WebDetection
{
    public class WebSimilarSearchAnnotationDto
    {
        public float Score { get; set; }
        public string Description { get; set; }

        public WebSimilarSearchAnnotationDto(float score, string description)
        {
            this.Score = score;
            this.Description = description;
        }
    }
}
