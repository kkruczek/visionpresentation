﻿using GoogleVision.Logic.Model.WebDetection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GoogleVision.Logic.Model
{
    public class WebDetectionDto
    {
        public List<WebSimilarSearchAnnotationDto> SimilarSearches { get; set; }

        public List<string> PagesWithMatchedImages { get; set; }
        public List<string> SameImagesUrls { get; set; }
        public List<string> SimilarImagesUrls { get; set; }

        public WebDetectionDto()
        {
            this.SimilarSearches = new List<WebSimilarSearchAnnotationDto>();
            this.PagesWithMatchedImages = new List<string>();
            this.SameImagesUrls = new List<string>();
            this.SimilarImagesUrls = new List<string>();
        }

        public WebDetectionDto(
            List<WebSimilarSearchAnnotationDto> similarSearches,
            List<string> matchedImages,
            List<string> sameIamges,
            List<string> similarImages)
        {
            this.SimilarSearches = similarSearches;
            this.PagesWithMatchedImages = matchedImages;
            this.SameImagesUrls = sameIamges;
            this.SimilarImagesUrls = similarImages;
        }
    }
}
