﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GoogleVision.Logic.Model.FaceDetection
{
    public class FaceDetectionDto
    {
        public List<FaceSquareDto> FaceSquare { get; set; }

        public FaceDetectionDto(List<FaceSquareDto> faceSquare)
        {
            this.FaceSquare = faceSquare;
        }
    }
}
