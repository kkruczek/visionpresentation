﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GoogleVision.Logic.Model.FaceDetection
{
    public class FaceSquareDto
    {
        public PointDto LeftTop { get; set; }
        public PointDto RightTop { get; set; }
        public PointDto LeftBottom { get; set; }
        public PointDto RightBottom { get; set; }

        public FaceSquareDto(PointDto leftTop, PointDto rightTop, PointDto leftBottom, PointDto rightBottom)
        {
            this.LeftTop = leftTop;
            this.RightTop = rightTop;
            this.LeftBottom = leftBottom;
            this.RightBottom = rightBottom;
        }
    }
}
