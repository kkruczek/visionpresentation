﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GoogleVision.Logic.Model
{
    public class LabelDetectionDto
    {
        public List<LabelAnnotationDto> Annotations { get; set; }

        public LabelDetectionDto()
        {
            this.Annotations = new List<LabelAnnotationDto>();
        }

        public LabelDetectionDto(List<LabelAnnotationDto> annotations)
        {
            this.Annotations = annotations;
        }
    }
}
