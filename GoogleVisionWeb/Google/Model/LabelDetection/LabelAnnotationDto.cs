﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GoogleVision.Logic.Model
{
    public class LabelAnnotationDto
    {
        public string Description { get; set; }
        public float Score { get; set; }

        public LabelAnnotationDto(string description, float score)
        {
            this.Description = description;
            this.Score = score;
        }
    }
}
