﻿using GoogleVision.Logic.Model;
using GoogleVision.Logic.Model.FaceDetection;
using GoogleVision.Logic.Model.SafeSearch;
using GoogleVision.Logic.Model.WebDetection;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace GoogleVision.Logic.Logic
{
    public class GoogleVisionPictureAnalysisLogic
    {

        public static List<GoogleVisionPictureAnalysisDto> _fakeDB;

        public GoogleVisionPictureAnalysisLogic()
        {
            if (_fakeDB == null)
            {
                string basePath = Path.Combine(Path.GetDirectoryName(AppDomain.CurrentDomain.BaseDirectory), @"bin\fakeFiles");
                _fakeDB = new List<GoogleVisionPictureAnalysisDto>()
                {
                    new GoogleVisionPictureAnalysisDto(
                        File.ReadAllBytes(Path.Combine(basePath, "face-06.jpg")),
                        "face-06.jpg",
                        new LabelDetectionDto(new List<LabelAnnotationDto>() {
                            new LabelAnnotationDto("Eyebrow", 0.98f),
                            new LabelAnnotationDto("Face", 0.98f),
                            new LabelAnnotationDto("Check", 0.95f),
                            new LabelAnnotationDto("Nose", 0.94f),
                        }),
                        new WebDetectionDto(
                            new List<WebSimilarSearchAnnotationDto>() {
                                new WebSimilarSearchAnnotationDto(5.35f, "Pro Evolution Soccer 2016"),
                                new WebSimilarSearchAnnotationDto(3.63f, "Pro Evolution Soccer 2017"),
                                new WebSimilarSearchAnnotationDto(0.88f, "Face")
                            },
                            new List<string> {
                                "http://elektroniksigaraankara.info/pixabay/rphoto/relaxed-face.html",
                                "http://robcampbellphotography.com/Grace-Face",
                                "http://fabriccreative.com/portfolio/photo-grace-face",
                                "https://alfa-img.com/show/relaxed-face.html",
                                "http://keywordsuggest.org/gallery/632725.html",
                                "https://www.peacequarters.com/princeton-psychologist-reveals-people-judge-based-face/",
                                "http://evome.co/princeton-psychologist-reveals-how-people-judge-you-based-on-your-face/",
                                "http://dreamicus.com/face.html"
                            },
                            new List<string> {
                                "https://i.ytimg.com/vi/RWl04IVGaoY/maxresdefault.jpg",
                                "https://pbs.twimg.com/media/CrZEbU1UIAAgUOi.jpg",
                                "http://wizardspark.com//wp-content/uploads/2016/12/img001.png",
                                "http://rynekpracy.wiosna.org.pl/inkubator/wp-content/uploads/sites/5/2016/08/indeks-3-155x155.jpg"
                            },
                            new List<string> {
                                "http://cartoonphotos.com/wp-content/uploads/2016/05/imageedit_0_9488492436.jpg",
                                "http://www.incrediblehearts.org/wp-content/uploads/2016/07/grace-face-1.jpg",
                                "http://cartoonphotos.com/wp-content/uploads/2016/05/imageedit_0_6903083343.jpg",
                                "http://cartoonphotos.com/wp-content/uploads/2016/05/imageedit_0_7165781054.jpg",
                                "https://i.ytimg.com/vi/RWl04IVGaoY/maxresdefault.jpg",
                                "http://arhivach.org/storage2/6/d6/6d697e1aa7c4fef3f63dd32868810870.jpg",
                                "http://www.drtalamas.com/wp-content/uploads/2016/12/Senosrenales-.jpg"
                            }),
                        new SafeSearchAnnotationDto(Likelihood.UNLIKELY, Likelihood.LIKELY, Likelihood.POSSIBLE, Likelihood.VERY_LIKELY),
                        null)
                        { ID = 1},
                    new GoogleVisionPictureAnalysisDto(
                        File.ReadAllBytes(Path.Combine(basePath, "HomeKONCEPT_51_zdjecie_1-842x418.jpg")),
                        "HomeKONCEPT_51_zdjecie_1-842x418.jpg",
                        new LabelDetectionDto(new List<LabelAnnotationDto>() {
                            new LabelAnnotationDto("House", 0.94f),
                            new LabelAnnotationDto("Property", 0.92f),
                            new LabelAnnotationDto("Home", 0.84f),
                            new LabelAnnotationDto("Archtecture", 0.83f),
                        }),
                         new WebDetectionDto(
                            new List<WebSimilarSearchAnnotationDto>() {
                                new WebSimilarSearchAnnotationDto(5.35f, "Pro Evolution Soccer 2016"),
                                new WebSimilarSearchAnnotationDto(3.63f, "Pro Evolution Soccer 2017"),
                                new WebSimilarSearchAnnotationDto(0.88f, "Face")
                            },
                            new List<string> {
                                "http://elektroniksigaraankara.info/pixabay/rphoto/relaxed-face.html",
                                "http://robcampbellphotography.com/Grace-Face",
                                "http://fabriccreative.com/portfolio/photo-grace-face",
                                "https://alfa-img.com/show/relaxed-face.html",
                                "http://keywordsuggest.org/gallery/632725.html",
                                "https://www.peacequarters.com/princeton-psychologist-reveals-people-judge-based-face/",
                                "http://evome.co/princeton-psychologist-reveals-how-people-judge-you-based-on-your-face/",
                                "http://dreamicus.com/face.html"
                            },
                            new List<string> {
                                "https://i.ytimg.com/vi/RWl04IVGaoY/maxresdefault.jpg",
                                "https://pbs.twimg.com/media/CrZEbU1UIAAgUOi.jpg",
                                "http://wizardspark.com//wp-content/uploads/2016/12/img001.png",
                                "http://rynekpracy.wiosna.org.pl/inkubator/wp-content/uploads/sites/5/2016/08/indeks-3-155x155.jpg"
                            },
                            new List<string> {
                                "http://cartoonphotos.com/wp-content/uploads/2016/05/imageedit_0_9488492436.jpg",
                                "http://www.incrediblehearts.org/wp-content/uploads/2016/07/grace-face-1.jpg",
                                "http://cartoonphotos.com/wp-content/uploads/2016/05/imageedit_0_6903083343.jpg",
                                "http://cartoonphotos.com/wp-content/uploads/2016/05/imageedit_0_7165781054.jpg",
                                "https://i.ytimg.com/vi/RWl04IVGaoY/maxresdefault.jpg",
                                "http://arhivach.org/storage2/6/d6/6d697e1aa7c4fef3f63dd32868810870.jpg",
                                "http://www.drtalamas.com/wp-content/uploads/2016/12/Senosrenales-.jpg"
                            }),
                        new SafeSearchAnnotationDto(Likelihood.UNLIKELY, Likelihood.LIKELY, Likelihood.POSSIBLE, Likelihood.VERY_LIKELY),
                        null)
                         { ID = 2},
                     new GoogleVisionPictureAnalysisDto(
                        File.ReadAllBytes(Path.Combine(basePath, "Homepage-Hero-Car.png")),
                        "Homepage-Hero-Car.png",
                        new LabelDetectionDto(new List<LabelAnnotationDto>() {
                            new LabelAnnotationDto("Car", 0.99f),
                            new LabelAnnotationDto("Aston Martin Vanatage", 0.97f),
                            new LabelAnnotationDto("Vehicle", 0.96f),
                            new LabelAnnotationDto("Sports Car", 0.94f),
                        }),
                         new WebDetectionDto(
                            new List<WebSimilarSearchAnnotationDto>() {
                                new WebSimilarSearchAnnotationDto(5.35f, "Pro Evolution Soccer 2016"),
                                new WebSimilarSearchAnnotationDto(3.63f, "Pro Evolution Soccer 2017"),
                                new WebSimilarSearchAnnotationDto(0.88f, "Face")
                            },
                            new List<string> {
                                "http://elektroniksigaraankara.info/pixabay/rphoto/relaxed-face.html",
                                "http://robcampbellphotography.com/Grace-Face",
                                "http://fabriccreative.com/portfolio/photo-grace-face",
                                "https://alfa-img.com/show/relaxed-face.html",
                                "http://keywordsuggest.org/gallery/632725.html",
                                "https://www.peacequarters.com/princeton-psychologist-reveals-people-judge-based-face/",
                                "http://evome.co/princeton-psychologist-reveals-how-people-judge-you-based-on-your-face/",
                                "http://dreamicus.com/face.html"
                            },
                            new List<string> {
                                "https://i.ytimg.com/vi/RWl04IVGaoY/maxresdefault.jpg",
                                "https://pbs.twimg.com/media/CrZEbU1UIAAgUOi.jpg",
                                "http://wizardspark.com//wp-content/uploads/2016/12/img001.png",
                                "http://rynekpracy.wiosna.org.pl/inkubator/wp-content/uploads/sites/5/2016/08/indeks-3-155x155.jpg"
                            },
                            new List<string> {
                                "http://cartoonphotos.com/wp-content/uploads/2016/05/imageedit_0_9488492436.jpg",
                                "http://www.incrediblehearts.org/wp-content/uploads/2016/07/grace-face-1.jpg",
                                "http://cartoonphotos.com/wp-content/uploads/2016/05/imageedit_0_6903083343.jpg",
                                "http://cartoonphotos.com/wp-content/uploads/2016/05/imageedit_0_7165781054.jpg",
                                "https://i.ytimg.com/vi/RWl04IVGaoY/maxresdefault.jpg",
                                "http://arhivach.org/storage2/6/d6/6d697e1aa7c4fef3f63dd32868810870.jpg",
                                "http://www.drtalamas.com/wp-content/uploads/2016/12/Senosrenales-.jpg"
                            }),
                        new SafeSearchAnnotationDto(Likelihood.UNLIKELY, Likelihood.LIKELY, Likelihood.POSSIBLE, Likelihood.VERY_LIKELY),
                        null)
                     { ID = 3},
                    new GoogleVisionPictureAnalysisDto(
                        File.ReadAllBytes(Path.Combine(basePath, "face-06.jpg")),
                        "face-06.jpg",
                        new LabelDetectionDto(new List<LabelAnnotationDto>() {
                            new LabelAnnotationDto("Eyebrow", 0.98f),
                            new LabelAnnotationDto("Face", 0.98f),
                            new LabelAnnotationDto("Check", 0.95f),
                            new LabelAnnotationDto("Nose", 0.94f),
                        }),
                        new WebDetectionDto(
                            new List<WebSimilarSearchAnnotationDto>() {
                                new WebSimilarSearchAnnotationDto(5.35f, "Pro Evolution Soccer 2016"),
                                new WebSimilarSearchAnnotationDto(3.63f, "Pro Evolution Soccer 2017"),
                                new WebSimilarSearchAnnotationDto(0.88f, "Face")
                            },
                            new List<string> {
                                "http://elektroniksigaraankara.info/pixabay/rphoto/relaxed-face.html",
                                "http://robcampbellphotography.com/Grace-Face",
                                "http://fabriccreative.com/portfolio/photo-grace-face",
                                "https://alfa-img.com/show/relaxed-face.html",
                                "http://keywordsuggest.org/gallery/632725.html",
                                "https://www.peacequarters.com/princeton-psychologist-reveals-people-judge-based-face/",
                                "http://evome.co/princeton-psychologist-reveals-how-people-judge-you-based-on-your-face/",
                                "http://dreamicus.com/face.html"
                            },
                            new List<string> {
                                "https://i.ytimg.com/vi/RWl04IVGaoY/maxresdefault.jpg",
                                "https://pbs.twimg.com/media/CrZEbU1UIAAgUOi.jpg",
                                "http://wizardspark.com//wp-content/uploads/2016/12/img001.png",
                                "http://rynekpracy.wiosna.org.pl/inkubator/wp-content/uploads/sites/5/2016/08/indeks-3-155x155.jpg"
                            },
                            new List<string> {
                                "http://cartoonphotos.com/wp-content/uploads/2016/05/imageedit_0_9488492436.jpg",
                                "http://www.incrediblehearts.org/wp-content/uploads/2016/07/grace-face-1.jpg",
                                "http://cartoonphotos.com/wp-content/uploads/2016/05/imageedit_0_6903083343.jpg",
                                "http://cartoonphotos.com/wp-content/uploads/2016/05/imageedit_0_7165781054.jpg",
                                "https://i.ytimg.com/vi/RWl04IVGaoY/maxresdefault.jpg",
                                "http://arhivach.org/storage2/6/d6/6d697e1aa7c4fef3f63dd32868810870.jpg",
                                "http://www.drtalamas.com/wp-content/uploads/2016/12/Senosrenales-.jpg"
                            }),
                        new SafeSearchAnnotationDto(Likelihood.UNLIKELY, Likelihood.LIKELY, Likelihood.POSSIBLE, Likelihood.VERY_LIKELY),
                        null)
                        { ID = 4},
                    new GoogleVisionPictureAnalysisDto(
                        File.ReadAllBytes(Path.Combine(basePath, "HomeKONCEPT_51_zdjecie_1-842x418.jpg")),
                        "HomeKONCEPT_51_zdjecie_1-842x418.jpg",
                        new LabelDetectionDto(new List<LabelAnnotationDto>() {
                            new LabelAnnotationDto("House", 0.94f),
                            new LabelAnnotationDto("Property", 0.92f),
                            new LabelAnnotationDto("Home", 0.84f),
                            new LabelAnnotationDto("Archtecture", 0.83f),
                        }),
                         new WebDetectionDto(
                            new List<WebSimilarSearchAnnotationDto>() {
                                new WebSimilarSearchAnnotationDto(5.35f, "Pro Evolution Soccer 2016"),
                                new WebSimilarSearchAnnotationDto(3.63f, "Pro Evolution Soccer 2017"),
                                new WebSimilarSearchAnnotationDto(0.88f, "Face")
                            },
                            new List<string> {
                                "http://elektroniksigaraankara.info/pixabay/rphoto/relaxed-face.html",
                                "http://robcampbellphotography.com/Grace-Face",
                                "http://fabriccreative.com/portfolio/photo-grace-face",
                                "https://alfa-img.com/show/relaxed-face.html",
                                "http://keywordsuggest.org/gallery/632725.html",
                                "https://www.peacequarters.com/princeton-psychologist-reveals-people-judge-based-face/",
                                "http://evome.co/princeton-psychologist-reveals-how-people-judge-you-based-on-your-face/",
                                "http://dreamicus.com/face.html"
                            },
                            new List<string> {
                                "https://i.ytimg.com/vi/RWl04IVGaoY/maxresdefault.jpg",
                                "https://pbs.twimg.com/media/CrZEbU1UIAAgUOi.jpg",
                                "http://wizardspark.com//wp-content/uploads/2016/12/img001.png",
                                "http://rynekpracy.wiosna.org.pl/inkubator/wp-content/uploads/sites/5/2016/08/indeks-3-155x155.jpg"
                            },
                            new List<string> {
                                "http://cartoonphotos.com/wp-content/uploads/2016/05/imageedit_0_9488492436.jpg",
                                "http://www.incrediblehearts.org/wp-content/uploads/2016/07/grace-face-1.jpg",
                                "http://cartoonphotos.com/wp-content/uploads/2016/05/imageedit_0_6903083343.jpg",
                                "http://cartoonphotos.com/wp-content/uploads/2016/05/imageedit_0_7165781054.jpg",
                                "https://i.ytimg.com/vi/RWl04IVGaoY/maxresdefault.jpg",
                                "http://arhivach.org/storage2/6/d6/6d697e1aa7c4fef3f63dd32868810870.jpg",
                                "http://www.drtalamas.com/wp-content/uploads/2016/12/Senosrenales-.jpg"
                            }),
                        new SafeSearchAnnotationDto(Likelihood.UNLIKELY, Likelihood.LIKELY, Likelihood.POSSIBLE, Likelihood.VERY_LIKELY),
                        null)
                         { ID = 5},
                     new GoogleVisionPictureAnalysisDto(
                        File.ReadAllBytes(Path.Combine(basePath, "Homepage-Hero-Car.png")),
                        "Homepage-Hero-Car.png",
                        new LabelDetectionDto(new List<LabelAnnotationDto>() {
                            new LabelAnnotationDto("Car", 0.99f),
                            new LabelAnnotationDto("Aston Martin Vanatage", 0.97f),
                            new LabelAnnotationDto("Vehicle", 0.96f),
                            new LabelAnnotationDto("Sports Car", 0.94f),
                        }),
                         new WebDetectionDto(
                            new List<WebSimilarSearchAnnotationDto>() {
                                new WebSimilarSearchAnnotationDto(5.35f, "Pro Evolution Soccer 2016"),
                                new WebSimilarSearchAnnotationDto(3.63f, "Pro Evolution Soccer 2017"),
                                new WebSimilarSearchAnnotationDto(0.88f, "Face")
                            },
                            new List<string> {
                                "http://elektroniksigaraankara.info/pixabay/rphoto/relaxed-face.html",
                                "http://robcampbellphotography.com/Grace-Face",
                                "http://fabriccreative.com/portfolio/photo-grace-face",
                                "https://alfa-img.com/show/relaxed-face.html",
                                "http://keywordsuggest.org/gallery/632725.html",
                                "https://www.peacequarters.com/princeton-psychologist-reveals-people-judge-based-face/",
                                "http://evome.co/princeton-psychologist-reveals-how-people-judge-you-based-on-your-face/",
                                "http://dreamicus.com/face.html"
                            },
                            new List<string> {
                                "https://i.ytimg.com/vi/RWl04IVGaoY/maxresdefault.jpg",
                                "https://pbs.twimg.com/media/CrZEbU1UIAAgUOi.jpg",
                                "http://wizardspark.com//wp-content/uploads/2016/12/img001.png",
                                "http://rynekpracy.wiosna.org.pl/inkubator/wp-content/uploads/sites/5/2016/08/indeks-3-155x155.jpg"
                            },
                            new List<string> {
                                "http://cartoonphotos.com/wp-content/uploads/2016/05/imageedit_0_9488492436.jpg",
                                "http://www.incrediblehearts.org/wp-content/uploads/2016/07/grace-face-1.jpg",
                                "http://cartoonphotos.com/wp-content/uploads/2016/05/imageedit_0_6903083343.jpg",
                                "http://cartoonphotos.com/wp-content/uploads/2016/05/imageedit_0_7165781054.jpg",
                                "https://i.ytimg.com/vi/RWl04IVGaoY/maxresdefault.jpg",
                                "http://arhivach.org/storage2/6/d6/6d697e1aa7c4fef3f63dd32868810870.jpg",
                                "http://www.drtalamas.com/wp-content/uploads/2016/12/Senosrenales-.jpg"
                            }),
                        new SafeSearchAnnotationDto(Likelihood.UNLIKELY, Likelihood.LIKELY, Likelihood.POSSIBLE, Likelihood.VERY_LIKELY),
                        null)
                     { ID = 6}
                };

            }
        }

        public void Add(GoogleVisionPictureAnalysisDto dto)
        {
            dto.ID = _fakeDB.Count == 0 ? 1 : (_fakeDB.Max(n => n.ID) + 1);
            _fakeDB.Add(dto);
        }

        public void Remove(GoogleVisionPictureAnalysisDto dto)
        {
            if (_fakeDB.Any(n => n.ID == dto.ID))
            {
                _fakeDB.Remove(dto);
            }
        }

        public IEnumerable<GoogleVisionPictureAnalysisDto> GetAll()
        {
            return _fakeDB;
        }

        public GoogleVisionPictureAnalysisDto GetSingle(int id)
        {
            if (_fakeDB.Any(n => n.ID == id))
            {
                return _fakeDB.First(n => n.ID == id);
            }
            return null;
        }
    }
}
