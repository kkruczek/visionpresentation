# README #

This poject is presenting the basic functionalities avaliable in the Google Vision API like:
 face square recognition,
 labels that describe uploaded picture,
 searching for a same/similar images in the web.

App allows to upload a file from disk and analyze it using Google Vision API, the data is stored in the static list (fake db), 
so its storing data only for a app lifetime.
(on first run list is initialized with list of pictures with fake data)

Project have been done using following functionalities:
 ASP.NET MVC + Web Api for backend,
 Knockout + bootstrap for frontend,
 For file upload dropzone.js,
 For images list Masonry.js